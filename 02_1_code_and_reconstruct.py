# -*- coding: utf-8 -*-1.3
# Copyright (C) 2019  Maximilian Schambach
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


"""
Reconstruct multispectral image from spectrally coded light field.
"""

import argparse
from datetime import date
import gc
import itertools
import json
import logging as lg
import os
import uuid

import imageio
import numpy as np
import pandas as pd
from skimage.measure import compare_psnr

from plenpy.lightfields import LightField

from support.support import shift, add_inplace_ignore_nan, fill_missing


# From Scene creation we found minimal distance:
g_min = 0.7620789513793631    # For integer valued disparities
# g_min = 0.7537044574081613    # For half integer valued disparities

# Argument setup
parser = argparse.ArgumentParser(
    description="Code and Reconstruct HS Light Fields.")

group = parser.add_mutually_exclusive_group(required=True)

group.add_argument('-F', '--folder', metavar='PATH',
                   nargs='+',
                   help='Folder path to hyperspectral light fields.')

group.add_argument('-f', '--file', metavar='PATH',
                   nargs='+',
                   help='Path to single light field.')

parser.add_argument('-o', '--output', type=str,
                    help='Output folder.')


args = parser.parse_args()

# Extract arguments
folder_path = args.folder
lf_path = args.file
output = args.output

if folder_path is None and lf_path is None:
    folder_path = os.path.abspath("./")
elif folder_path is None and lf_path is not None:
    folder_path = os.path.dirname(lf_path[0])
else:
    folder_path = os.path.abspath(folder_path[0])

# Get output CSV name
if output is None:
    output = folder_path

else:
    # Create subfolder if non existent
    if not os.path.exists(output):
        os.makedirs(output)

csv_filename = os.path.join(
        output,
        f"reconstruction_{str(uuid.uuid4())[:8]}_{date.today().isoformat()}.csv")

# Logger set up
logger = lg.getLogger("code-and-reconstruct")
logger.setLevel(lg.INFO)
FORMAT = "%(levelname)s: %(message)s"
lg.basicConfig(format=FORMAT)

# Create pandas data frame to save results
cols = ['filepath', 'scene', 'sensor_size', 'pixel_pitch', 'num_channels', 'focal_length',
        'f_number', 'r_main_lens', 'r_microlens', 'num_u_lenses', 'num_v_lenses',
        'disparity', 'distance', 'num_ang_step',
        'mask', 'perc_reconstructed', 'PSNR', 'MAE']
df = pd.DataFrame([], columns=cols)

# Read light fields
files = []

if lf_path is None:
    logger.info(f"Reading light fields from folder {folder_path}")
    for file in sorted(os.listdir(folder_path)):
        # Check that the file is a readable image file
        if file.lower().endswith("img"):
            files.append(
                os.path.join(folder_path, file)
            )

else:
    logger.info(f"Reading light field file {lf_path}")
    files = lf_path

curr_file = 0
for file in files:
    curr_file += 1

    logger.info(f"Processing file {curr_file} of {len(files)}")
    lf_name = os.path.splitext(os.path.basename(file))[0] + ".metadata"

    logger.info(f"Reading metadata")
    with open(os.path.join(folder_path, lf_name)) as json_file:
        metadata = json.load(json_file)

    scene = metadata['scene']
    size_x = metadata['sensorSizeX']
    size_y = metadata['sensorSizeY']
    sensor_size = np.asarray([size_x, size_y])
    del size_x, size_y
    pixel_pitch = metadata['pixelPitch']
    focal_length = metadata['focalLength']
    f_number = metadata['fNumber']
    g = metadata['distance']
    disp = int(metadata['disparity'])
    num_ang_step_full = metadata['numAngStep']
    num_u = metadata['numULenses:']
    num_v = metadata['numVLenses:']
    r_microlens = metadata['microLensRadius:']
    r_main_lens = 0.5 * focal_length / f_number

    logger.info(f"Reading light field")
    lf_gt_full = LightField.from_file(file, num_u, num_v, format='GDAL')
    num_ch = lf_gt_full.num_channels

    logger.info(f"Cropping light field")
    crop = 1
    num_ang_step = num_ang_step_full - 2*crop
    lf_gt = LightField(lf_gt_full[crop:(num_ang_step + crop), crop:(num_ang_step + crop)], copy=True)

    del lf_gt_full
    gc.collect()

    for mask_type in ['random', 'regular']:
        logger.info(f"Color coding light field with {mask_type} mask.")
        lf_cc, mask = lf_gt.get_colorcoded_copy(method=mask_type, return_mask=True)

        # Initialize HS image of central view
        center = num_ang_step//2
        hsi_data = lf_cc[center, center].copy()
        hsi_data_gt = lf_gt[center, center]  # Ground Truth
        mask_sum = mask.copy()

        logger.info(f"Shifting subapertures")
        # Fill up data
        for u, v in itertools.product(range(num_ang_step), range(num_ang_step)):
            # Ignore (middle, middle) component
            if not [u, v] == [center, center]:

                dist = np.asarray([u, v]) - np.asarray([center, center])
                shift_amt = (disp * dist[0], disp * dist[1])

                # Shift subaperture to central view
                shifted = shift(lf_cc[u, v], shift=shift_amt)

                # Add to central view, ignore nan values
                add_inplace_ignore_nan(hsi_data, shifted)

                # Count number of non nan values
                mask_shifted = shift(mask, shift=shift_amt)

                add_inplace_ignore_nan(mask_sum, mask_shifted)

        # Divide by number of summands
        data_is_nan = np.isnan(hsi_data)
        hsi_data[~data_is_nan] /= mask_sum[~data_is_nan]

        # Percentage of reconstructed values
        num_missing = np.sum(data_is_nan)
        rec_amount = 1 - num_missing / hsi_data.size

        logger.info("Reconstructed {} % of the pixels.".format(100 * rec_amount))

        # Replace missing data by in-place nearest neighbor interpolation
        # in spectral direction

        logger.info("Filling missing values by interpolation")
        NAN_tot = np.where(np.isnan(hsi_data))[0].size

        # Interpolate only if sufficient data is available
        if rec_amount > 0.25:
            fill_missing(hsi_data, num_ch)

        NAN_replaced = NAN_tot - np.where(np.isnan(hsi_data))[0].size
        logger.info("Interpolated {} of {} NANs.".format(NAN_replaced, NAN_tot))

        # Replace last NANs
        hsi_data[np.where(np.isnan(hsi_data))] = 0.5

        # Calculate PSNR in always the same field of view
        # as the scene with the smallest distance
        x_0, y_0 = (-np.array([num_u, num_v])/2 * (g_min / g - 1)).astype(np.int32)
        x_1, y_1 = (np.array([num_u, num_v])/2 * (g_min / g + 1)).astype(np.int32)

        PSNR = compare_psnr(hsi_data_gt[x_0:x_1, y_0:y_1], hsi_data[x_0:x_1, y_0:y_1])
        MAE_img = np.sum(np.abs(hsi_data_gt[x_0:x_1, y_0:y_1] - hsi_data[x_0:x_1, y_0:y_1]), axis=-1) / num_ch
        MAE = np.mean(MAE_img)

        # Save mean square error image
        mae_img_filename = os.path.join(
            output, os.path.splitext(os.path.basename(file))[0] +"_" + mask_type +"_MAE.png")
        imageio.imwrite(mae_img_filename, MAE_img)

        logger.info("Done. Reconstruction PSNR: {} dB".format(round(PSNR, 2)))

        # Add new data to DataFrame
        df = df.append(
            pd.DataFrame([[os.path.basename(file), scene, sensor_size, pixel_pitch, num_ch, focal_length,
                           f_number, r_main_lens, r_microlens, num_u, num_v,
                           disp, g, num_ang_step, mask_type,
                           rec_amount, PSNR, MAE]],
                         columns=cols))

        # np.save("/home/schambach/Desktop/hsi_norm.npy", hsi_data)

        # Clean Up
        del hsi_data, hsi_data_gt, data_is_nan, lf_cc
        gc.collect()

    # Clean up, file finished
    del lf_gt
    gc.collect()

    # Redundant save in case of error
    df.to_csv(csv_filename, index=False)

# Save dataframe
df.to_csv(csv_filename, index=False)
