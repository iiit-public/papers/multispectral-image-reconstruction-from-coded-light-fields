# Reconstruction of multispectral images from spectrally coded light fields of flat scenes

Source code accompanying the paper 

    Maximilian Schambach and Fernando Puente León
    Reconstruction of multispectral images from spectrally coded light fields of flat scenes
    tm -Technisches Messen
    Online Preprint at https://doi.org/10.1515/teme-2019-0103



To recreate the evaluation presented in the paper, follow these steps.


## Preparation

### Dependencies
You will need Python 3.6 or newer to run the provided code.
Furthermore, the code depends on our provided Python 
package [plenpy](https://gitlab.com/iiit-public/plenpy). You can intall it simply via

    $ pip install plenpy
    
It is recommended to install the package to a new environment.

To render the created scenes, use our provided [raytracer](https://gitlab.com/iiit-public/raytracer).
For details, please refer to its [official documentation](https://iiit-public.gitlab.io/raytracer/).



## 1: Scene creation
The programm ``01_create_scenes.py`` creates the JSON scene discription that 
serve as the input for the [raytracer](https://gitlab.com/iiit-public/raytracer).
You can pass options to the pogram via the command line, run

    $ python 01_create_scenes --help
    
to view the program help. Using the according options, you can set different
values for the scene, camera, etc.
If you run the program without any explicit options, the default values are used
which correspond to the values used in our paper.

The JSON files, together with some metadata, are saved to the ``Scenes`` folder
if not specified otherwise. Please, render all scenes using the raytracer.

*NOTE*: The provided geamtry description files (`Scenes/Geometry/*.json`) rely
on multispectral textures that we do not supply in this repository. 
The used multispectral images are taken from publicly available datasets such as
the [CAVE Project](http://www1.cs.columbia.edu/CAVE/databases/multispectral/) and
converted to Nump'y data format.
Please either supply your own geometry descriptions or convert the necessary
multispectral images to ``.npy``.


## 2: Code and reconstruct
The program ``02_estimate_grid.py`` uses the rendered multispectral light fields 
to create a spectrally coded light field from it and reconstruct the multispectral central view
using the provided metadata files. The light field must have an integer valued disparity.

Again, see

    $ python 02_estimate_grid.py --help
    
for help on the possible options. Usually, you can simply run it, pointing
to the folder containing all rendered images (as `.img` hyperspectral images in the ENVI format) together with their
metadata (`.metadata`) files, running

    $ python 02_estimate_grid.py --folder <path-to-lightfield-folder>
    
Using the programm ``02_estimate_grid.py``, the reconstruction is perfomed
with superresolution. This needs half integer valued disparities.

## Evaluation

For the evaluation of the grid estimation performance, feel free to use our
Jupyter notebook ``03_evaluate.ipynb``. 
You will have to adapt some folder paths in the first blocks, but the file is commented and self explanatory.
