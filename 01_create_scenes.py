# -*- coding: utf-8 -*-1.3
# Copyright (C) 2019  Maximilian Schambach
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


"""
Script to create json scenes for specified optimal camera parameters.
"""

import argparse
import datetime
import itertools
import json
import shutil
import os

import numpy as np

from support.support import get_cam_parameters, get_camera_json, \
    get_scene_json, compose_json, get_filename

# Argument setup
parser = argparse.ArgumentParser(
    description="Create JSON scene files for a camera.")

parser.add_argument('-C', '--camera', type=str,
                    choices=['thinlens', 'lightfield'],
                    default='lightfield', nargs=1,
                    help='Camera type.')

parser.add_argument('-s', '--scene', type=str,
                    choices=['cd', 'curry', 'grafitti', 'concrete', 'colorchart'],
                    default=['cd', 'curry', 'grafitti', 'concrete', 'colorchart'], nargs='+',
                    help='Scene geometry.')

parser.add_argument('-i', '--imagetype', type=str,
                    choices=['IdealRgb', 'MultiSpectral'],
                    default='MultiSpectral', nargs=1,
                    help='Image type of rendered image.')

parser.add_argument('-S', '--spectrumtype', type=str,
                    choices=['Monochromatic', 'RgbSpectrum', 'SampledSpectrum'],
                    default='SampledSpectrum', nargs=1,
                    help='Spectrum type of rendering process.')

parser.add_argument('-o', '--output', metavar='PATH',
                    default="./Scenes/test",
                    help='Output folder path.')

parser.add_argument('-f', '--folder', metavar='PATH',
                    default="./",
                    help='Input folder path.')

parser.add_argument('-r', '--range', type=float,
                    default=[0.75, 1.25], nargs=2,
                    help='Distance range to check in meters.')

parser.add_argument('--focal', type=float,
                    default=[50], nargs='+',
                    help='Focal length of lens in mm.')

parser.add_argument('--fnum', type=float,
                    default=[1.4, 1.8], nargs='+',
                    help='F-Numbers to check.')

parser.add_argument('-N', '--numangstep', type=int,
                    default=[5, 7, 9, 11, 13, 15], nargs='+',
                    help='Angular resolution to check '
                         '(number of subapertures per dimension).')

parser.add_argument('-D', '--disp', type=int,
                    # default=[0.5, 1.5, 2.5, 3.5, 4.5, 5.5, 6.5, 7.5],
                    default=[1, 2, 3, 4, 5, 6, 7],
                    nargs='+', help='Disparities to check')

parser.add_argument('--sensor', type=str,
                    choices=['ON-PYTHON-25K', 'ON-KAI-29050'],
                    default='ON-PYTHON-25K', nargs=1,
                    help='Sensor type.')

args = parser.parse_args()

# Extract arguments

# RayTracer options
scene_s = args.scene
image_type = args.imagetype
spectrum_type = args.spectrumtype

# Camera
camera = args.camera
sensor = args.sensor

dist_range = np.asarray(args.range)
focal_length_s = np.asarray(args.focal) * 10**-3  # mm to m
f_number_s = args.fnum
num_ang_step_s = args.numangstep
disp_s = args.disp

# System paths
folder_in = os.path.abspath(args.folder)
folder_out = os.path.abspath(args.output)

# Create subfolder if non existent
if not os.path.exists(folder_out):
    os.makedirs(folder_out)


# Get sensor properties from name
if sensor == "ON-PYTHON-25K":
    sensor_size = np.asarray([5120, 5120])
    sensor_pixelpitch = 4.5e-6

elif sensor == "ON-KAI-29050":
    sensor_size = np.asarray([6576, 4384])
    sensor_pixelpitch = 5.5e-6

else:
    raise ValueError("Unknown sensor.")


# Iterate over all possible parameter combinations
g_list = []
counter = 0
for disp, focal_length, f_num, num_ang_step in itertools.product(
        disp_s, focal_length_s, f_number_s, num_ang_step_s):

    # Calculate missing camera parameters in dist_range
    res = get_cam_parameters(disp, focal_length, f_num, num_ang_step,
                             sensor_size, sensor_pixelpitch, dist_range)

    # If result was in dist_range
    if res is not None:
        counter += 1
        g, r_main_lens, r_micro_lens, num_u, num_v = res

        for scene in scene_s:

            # Load scene
            scene_json = get_scene_json(scene)

            # Create camera json string
            camera_json = get_camera_json(
                camera, image_type, focal_length, sensor_size, sensor_pixelpitch,
                g, r_main_lens, r_micro_lens, num_u, num_v, num_ang_step)

            # Compose full json string
            full_json = compose_json(scene_json, camera_json, spectrum_type)

            # Write file to disk
            filename = get_filename(scene, sensor, camera, f_num, disp, num_ang_step)
            file = open(os.path.join(folder_out, filename + ".json"), 'w')

            # Write JSON file to disk and close
            file.write(full_json)
            file.close()

            g_list.append(g)

            # Save the Metadata in JSON format
            meta_dict = {
                "date": str(datetime.datetime.now()),
                "raytracerJsonfile": filename + ".json",
                "scene": scene,
                "camera": camera,
                "sensor": sensor,
                "sensorSizeX": int(sensor_size[0]),
                "sensorSizeY": int(sensor_size[1]),
                "pixelPitch": sensor_pixelpitch,
                "focalLength": focal_length,
                "fNumber": f_num,
                "distance": g,
                "disparity" : disp,
                "numAngStep": num_ang_step
            }

            if camera == "lightfield":
                meta_dict['numULenses:'] = num_u
                meta_dict['numVLenses:'] = num_v
                meta_dict['microLensRadius:'] = r_micro_lens

            json.dump(
                meta_dict,
                open(os.path.join(folder_out, filename + ".metadata"), 'w'),
                indent=4)

if counter == 0:
    raise ValueError("No valid parameters found...")

g_list = np.asarray(g_list)
print("Found combinations: ", g_list.size)
print("Minimal Distance: ", g_list.min())
print("Maximal Distance: ", g_list.max())