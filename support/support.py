"""Support functions for create_scenes script."""

import itertools
from typing import Tuple
import uuid
import os

import numpy as np
from numpy.core import ndarray
import pandas as pd
from scipy.interpolate import interp1d

# Number of samples used in raytracer
NUM_SAMPLES = 196


def get_cam_parameters(
        disp: int,
        focal_length: float,
        f_num: float,
        num_ang_step: int,
        sensor_size: ndarray,
        sensor_pixelpitch:float,
        dist_range: ndarray):

    # Distance
    g = focal_length ** 2 / (
                f_num * sensor_pixelpitch * disp * num_ang_step ** 2)

    # Main lens radius
    r_main_lens = 0.5 * focal_length / f_num

    # Microlens radius
    r_micro_lens = 0.5 * sensor_pixelpitch * num_ang_step

    # Number of microlenses
    num_u = int(sensor_size[0] / num_ang_step)
    num_v = int(sensor_size[1] / num_ang_step)

    if dist_range[0] < g < dist_range[1]:
        return g, r_main_lens, r_micro_lens, num_u, num_v

    else:
        return None


def get_camera_json(camera, image_type, focal_length, sensor_size, sensor_pixelpitch,
                    g, r_main_lens, r_micro_lens, num_u, num_v, num_ang_step):

    res = '"camera" : {\n'
    res += '\t"imageType" : "' + image_type + '",\n'

    res += '\t"eye" : [0.0, ' + str(-g) + ', 0.0],\n'
    res += '\t"lookAt": [0.0, 0.0, 0.0],\n'
    res += '\t"up": [0.0, 0.0, 1.0],\n'

    res += '\t"lensRadius" : ' + str(r_main_lens) + ',\n'
    res += '\t"imageDistance" : ' + str(focal_length) + ',\n'

    res += '\t"sampler": {\n'
    res += '\t\t"type": "MultiJittered",\n'
    res += '\t\t"numSamples": ' + str(NUM_SAMPLES) + '\n'
    res += '\t},\n'

    # Thin Lens Camera
    if camera == "thinlens":
        # Camera Type
        res += '\t"type" : "ThinLens",\n'

        res += '\t"focusDistance" : ' + str(g) + ',\n'

        # View Plane Size and Sampler
        res += '\t"viewPlane": {\n'
        res += '\t\t"resolution": ' + np.array2string(sensor_size, separator=', ') + ',\n'
        res += '\t\t"pixelSize": ' + str(sensor_pixelpitch) +',\n'

        res += '\t\t"sampler": {\n'
        res += '\t\t\t"type": "MultiJittered",\n'
        res += '\t\t\t"numSamples": ' + str(NUM_SAMPLES) + '\n'
        res += '\t\t}\n'

        res += '\t}\n'

    # Light Field Camera
    elif camera == 'lightfield':
        # Camera Type
        res += '\t"type" : "LightFieldReference",\n'

        res += '\t"numULenses" : ' + str(num_u) + ',\n'
        res += '\t"numVLenses" : ' + str(num_v) + ',\n'
        res += '\t"numAngSteps" : ' + str(num_ang_step) + ',\n'

        res += '\t"microLensRadius" : ' + str(r_micro_lens) + ',\n'

        res += '\t"focusDistance" : 9999,\n'

        # View Plane Sampler
        res += '\t"viewPlane": {\n'
        res += '\t\t"sampler": {\n'
        res += '\t\t\t"type": "MultiJittered",\n'
        res += '\t\t\t"numSamples": ' + str(NUM_SAMPLES) + '\n'
        res += '\t\t}\n'

        res += '\t}'

    else:
        raise ValueError("Unknown camera type.")

    res += '},\n'
    return res


def get_scene_json(scene):
    json = open(os.path.abspath("./Scenes/Geometry/" + scene + ".json"), "r")
    return json.read()


def compose_json(scene_json:str , camera_json: str, spectrum_type) -> str:
    res = '{\n'
    res += scene_json
    res+= '	"lights": [{"type": "AmbientLight", "radianceScaling": 1.0, "color": [255, 255, 255]}],\n\n'
    res += camera_json
    res += '"spectrumType" : "' + spectrum_type + '"'
    res += '\n}'

    return res


def get_filename(scene, sensor, camera, f_num, disp, num_ang_step):

    res = scene + "_" + sensor + "_" + camera + "_"
    res += "f-" + str(int(f_num)) + "-" + str(int(round(10*(f_num - int(f_num)),1))) + "_"
    res += "disp-" +  str(int(disp)) + "-" + str(int(round(10*(disp - int(disp)),1))) + "_numAngStep-" + str(num_ang_step)
    res += "_ID-" + str(uuid.uuid4())[:8]
    return res


def shift(input: ndarray, shift: Tuple[int, int]) -> ndarray:
    """Integer shift input along two axes."""

    # Shift axis 0
    out = np.roll(input, shift[0], axis=0)
    if shift[0] > 0:
        out[0:shift[0], :] = np.nan
    elif shift[0] < 0:
        out[shift[0]:, :] = np.nan

    # Shift axis 1
    out = np.roll(out, shift[1], axis=1)
    if shift[0] > 0:
        out[:, 0:shift[1]] = np.nan
    elif shift[0] < 0:
        out[:, shift[0]:] = np.nan

    return out


def add_inplace_ignore_nan(a, b):
    """Adds b to a in place ignoring NANs"""

    # Get NAN indices
    a_nans = np.isnan(a)
    b_nans = np.isnan(b)

    # Replace NANs
    a[a_nans] = 0.0
    b[b_nans] = 0.0

    # Add in place
    a += b

    # Place NANs where both a and b were NAN
    a_nans &= b_nans
    a[a_nans & b_nans] = np.nan

    return


def fill_missing(input, num_ch):

    """Fill NAN values by 1D-linear interpolation along spectral axis."""
    x_arr = np.arange(num_ch)

    # Find out indices, where interpolation is needed
    x_cords, y_cords = np.where(np.sum(np.isnan(input), axis=-1) != 0)

    for x, y in zip(x_cords, y_cords):
        id = np.isnan(input[x, y])

        try:
            f = interp1d(x_arr[~id], input[x, y][~id],
                         kind='linear', axis=-1, fill_value='extrapolate')
            input[x, y, np.where(id)[0]] = np.clip(f(np.where(id)[0]), 0, 1)

        # If too few values:
        except ValueError:
            pass

    return
